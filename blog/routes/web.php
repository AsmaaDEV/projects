<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin-login', [
    'uses' => 'Admin\AuthController@showLoginForm',
    'as' => 'admin.login',
]);

Route::post('/admin-login', [
    'uses' => 'Admin\AuthController@login',
    'as' => 'admin.post.login',
]);

Route::get('/logout', [
    'uses' => 'Admin\AuthController@logout',
    'as' => 'logout',
]);

Route::group(['namespace' => 'Web'], function () {

    Route::resource('categories', 'CatrgoriesController');

    Route::resource('posts', 'PostsController');

    Route::get('/', [
        'uses' => 'HomeController@index',
        'as' => 'home',
    ]);
});

Route::group(['middleware' => 'admin', 'prefix' => 'dashboard', 'namespace' => 'Admin'], function () {

    Route::get('/', [
        'uses' => 'HomeController@dashboard',
        'as' => 'dashboard',
    ]);
    
    Route::resource('category', 'CatrgoriesController');

    Route::resource('post', 'PostsController');
});