<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Charts;

class HomeController extends Controller
{
    public function dashboard()
    {
        $categoriesCount = Category::count();
        $postsCount = Post::count();

        return view('admin.dashboard.dashboard', compact('categoriesCount', 'postsCount'));
    }
}