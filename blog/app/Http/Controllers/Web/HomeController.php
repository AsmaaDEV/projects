<?php

namespace App\Http\Controllers\Web;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $posts = Post::latest()->paginate(5);

        return view('web.home', compact('posts'));

    }
}
