<?php

namespace App\Http\Controllers\Web;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CatrgoriesController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $posts = Post::where('category_id', $category->id)
                        ->latest()
                        ->paginate(5);
    
        return view('web.categories.show', compact('category', 'posts'));
                        
    }
}
