<?php


return [
    'home' => 'Home',
    'dashbord' => 'Dashbord',
    'loginPageTitle' => 'Admin login',
    'logout' => 'logout',
    'loginEmail' => 'email',
    'loginPassword' => 'password',
    'loginSubmit' => 'login',
    'dashboardTitle' => 'Admin Dashboard',
    'statistics' => 'Statistics',
    'statisticsView' => 'show statistics',
    'statisticsCharts' => 'Statistics Charts',
    'categoriesCount' => 'Categories Count',
    'postsCount' => 'Posts Count ',
    'add' => 'Add new',
	'create' => 'Create',    
    'show' => 'Show',
	'view' => 'view',    
    'edit' => 'Edit',
    'delete' => 'Delete',
    'update' => 'Update',
    'save' => 'Save',
    'list' => 'List',
    'no_entries_in_table' => 'No entries in table',
    'custom_controller_index' => 'Custom controller index.',
    'are_you_sure' => 'Are you sure?',
    'back_to_list' => 'Back to list',
    'categories' => 'Categories',
    'showCategories' => 'All Categories',
    'addCategories' => 'Add Category',
    'categoryName' => 'Category Name',
    'posts' => 'Posts',
    'showPosts' => 'All Posts',
    'addPosts' => 'Add Post',
    'title' => 'Title',
    'description' => 'Description',
    'content' => 'Content',


    
];