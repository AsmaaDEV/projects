<?php


return [
    'loginFalse' => 'wrong email or password',
    'addOk' => 'Successfully added',
    'addNO' => 'Not added Try again',
    'deletedOk' => 'Successfully deleted',
    'deletedNo' => 'Error,not deleted Try again',
    'updateTrue' => 'Updated successfully',
    'updateFalse' => 'Error, Not Updated Try again',
];