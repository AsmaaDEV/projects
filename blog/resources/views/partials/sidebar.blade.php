@inject('request', 'Illuminate\Http\Request')
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <li class="nav-item">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-home"></i>
                    <span class="title">{{ trans('admin.home') }}</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start ">
                        <a href="{{ route('dashboard') }}" class="nav-link ">
                            <span class="title">{{ trans('admin.statistics') }}</span>
                        </a>
                    </li>
                </ul>
            </li> 

            <li class="nav-item {{ strpos(URL::current(), 'category') !== false ? 'active' : '' }} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-sitemap"></i>
                    <span class="title">{{ trans('admin.categories') }}</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start ">
                        <a href="{{ route('category.index') }}" class="nav-link ">
                            <span class="title"> {{ trans('admin.showCategories') }} </span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="{{ route('category.create') }}" class="nav-link ">
                            <span class="title"> {{ trans('admin.addCategories') }}   </span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item {{ strpos(URL::current(), 'post') !== false ? 'active' : '' }} ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="fa fa-sticky-note-o"></i>
                    <span class="title">{{ trans('admin.posts') }} </span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start ">
                        <a href="{{ route('post.index') }}" class="nav-link ">
                            <span class="title">{{ trans('admin.showPosts') }} </span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="{{ route('post.create') }}" class="nav-link ">
                            <span class="title"> {{ trans('admin.addPosts') }} </span>
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
</div>
{!! Form::open(['route' => 'logout', 'style' => 'display:none;', 'id' => 'logout']) !!}
<button type="submit">{{ trans('admin.logout') }}</button>
{!! Form::close() !!}