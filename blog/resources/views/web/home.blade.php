@extends('web.app')

@section('content')

    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          <h1 class="my-4">All Posts</h1>
          @foreach ($posts as $post)              
            <!-- Blog Post -->
            <div class="card mb-4">
              <div class="card-body">
                <a href="{{ route('categories.show',[$post->category->id]) }}" ]>{{ $post->category->name }}</a>
                <h2 class="card-title">{{ $post->title }}</h2>
                <p class="card-text"> {{ str_limit($post->description , 150) }}</p>
                <a href="{{ route('posts.show',[$post->id]) }}" class="btn btn-primary">Show details &rarr;</a>
              </div>
              <div class="card-footer text-muted">
                {{ $post->created_at }}
              </div>
            </div>
          @endforeach
          <!-- Pagination -->
          <ul class="pagination justify-content-center mb-4">
            <li class="page-item">
              {{ $posts->links() }}
            </li>
          </ul>

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
@stop   
