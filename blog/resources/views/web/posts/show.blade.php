@extends('web.app')

@section('content')

    <!-- Page Content -->
    <div class="container">

      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-12">

          <h1 class="my-4">Post details</h1>
            <!-- Blog Post -->
            <div class="card mb-4">
              <div class="card-body">
                <a href="{{ route('categories.show',[$post->category->id]) }}" ]>{{ $post->category->name }}</a>
                <h2 class="card-title">{{ $post->title }}</h2>
                <h4 class="card-text"> {{ $post->content }}</h4>
                <p class="card-text"> {{ $post->description }}</p>
              </div>
              <div class="card-footer text-muted">
                {{ $post->created_at }}
              </div>
            </div>
        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
@stop   
