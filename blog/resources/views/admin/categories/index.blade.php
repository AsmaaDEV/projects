@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('admin.categories')</h3>
    <p>
        <a href="{{ route('category.create') }}" class="btn btn-success">@lang('admin.add')</a>
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('admin.list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($categories) > 0 ? 'datatable' : '' }}  dt-select">
                <thead>
                    <tr>
                        <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>
                        <th>@lang('admin.categoryName')</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($categories) > 0)
                        @foreach ($categories as $category)
                            <tr data-entry-id="{{ $category->id }}">
                                <td></td>                                
                                <td>{{ $category->name }}</td>
                                <td>
                                    <a href="{{ route('category.edit',[$category->id]) }}" class="btn btn-xs btn-info">@lang('admin.edit')</a>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("admin.are_you_sure")."');",
                                        'route' => ['category.destroy', $category->id])) !!}
                                    {!! Form::submit(trans('admin.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
        
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8">@lang('admin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop
