@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.tips.title')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('quickadmin.tips.fields.title')</th>
                            <td>{{ $tip->title }}</td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.tips.fields.photo')</th>
                            <td><img  src="{{  asset('uploads/tips/'. $tip->photo)}}"style="width: 70px; height: 70px;"></td>
                        </tr>
                        <tr>
                            <th>@lang('quickadmin.tips.fields.content')</th>
                            <td>{{ $tip->content }}</td>
                        </tr>
                    </table>
                </div>
            

            <p>&nbsp;</p>

            <a href="{{ route('admin.tips.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
    </div>
@stop