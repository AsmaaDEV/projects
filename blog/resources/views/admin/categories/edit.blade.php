@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('admin.categories')</h3>
    
    {!! Form::model($category, ['method' => 'PUT', 'route' => ['category.update', $category->id]]) !!}
        <div class="panel panel-default">
            <div class="panel-heading">
                @lang('admin.create')
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 form-group">
                        {!! Form::label('Name', 'name', ['class' => 'control-label']) !!}
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'name']) !!}
                    </div>
                </div>
            </div>
        </div>

    {!! Form::submit(trans('admin.update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

