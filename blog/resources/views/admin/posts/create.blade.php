@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('admin.posts')</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['post.store'] , 'files' => 'true']) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('admin.create')
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    <label for="name" class="col-lg-3 control-label">@lang('admin.categoryName')   </label>
                    {!!Form::select('category_id', $categories, null, ['class' => 'form-control category', 'placeholder' => ' -- select category -- '] ) !!}                               
                </div>
            </div>
        
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('Title', 'title', ['class' => 'control-label']) !!}
                    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'title']) !!}
                </div>
            </div>
           
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('Content', 'content', ['class' => 'control-label']) !!}
                    {!! Form::textarea('content', null,['class' => 'form-control autosizeme','placeholder' => 'content', 'rows' => '6', 'required']) !!}
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group ">
                    {!! Form::label('Description', 'description', ['class' => 'control-label']) !!}
                    {!! Form::textarea('description',null ,['class' => 'form-control autosizeme','placeholder' => 'description' , 'rows' => '10', 'required']) !!}
                </div>
            </div>
            
            
        </div>
    </div>

    {!! Form::submit(trans('admin.save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

