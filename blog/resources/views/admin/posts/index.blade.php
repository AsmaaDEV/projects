@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('admin.posts')</h3>
    <p>
        <a href="{{ route('post.create') }}" class="btn btn-success">@lang('admin.add')</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('admin.list')
        </div>

        <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped {{ count($posts) > 0 ? 'datatable' : '' }} dt-select ">
                <thead>
                    <tr>
                        <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>
                        <th>@lang('admin.categoryName')</th>
                        <th>@lang('admin.title')</th>
                        <th>@lang('admin.content')</th>
                        <th>@lang('admin.description')</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if (count($posts) > 0)
                        @foreach ($posts as $post)
                            <tr data-entry-id="{{ $post->id }}">
                                    <td></td>
                                <td>{{ $post->category->name }}</td>
                                <td>{{ $post->title }}</td>
                                <td>{{ str_limit($post->content , 20) }}</td>
                                <td>{{ str_limit($post->description , 25) }}</td>
                                <td>
                                    <a href="{{ route('post.show',[$post->id]) }}" class="btn btn-xs btn-primary">@lang('admin.view')</a>
                                    <a href="{{ route('post.edit',[$post->id]) }}" class="btn btn-xs btn-info">@lang('admin.edit')</a>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("admin.are_you_sure")."');",
                                        'route' => ['post.destroy', $post->id])) !!}
                                    {!! Form::submit(trans('admin.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8">@lang('admin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop
