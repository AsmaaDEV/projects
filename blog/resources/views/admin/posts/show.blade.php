@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('admin.posts')</h3>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('admin.view')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>@lang('admin.categoryName')</th>
                            <td>{{ $post->category->name }}</td>
                        </tr>
                        <tr>
                            <th>@lang('admin.title')</th>
                            <td>{{ $post->title }}</td>
                        </tr>
                        <tr>
                            <th>@lang('admin.content')</th>
                            <td>{{ $post->content }}</td>
                        </tr>
                        <tr>
                            <th>@lang('admin.description')</th>
                            <td>{{ $post->description }}</td>
                        </tr>
                    </table>
                </div>
    
            <p>&nbsp;</p>

            <a href="{{ route('post.index') }}" class="btn btn-default">@lang('admin.back_to_list')</a>
    </div>
@stop