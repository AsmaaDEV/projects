@extends('layouts.app')

@section('title')
    {{ trans('admin.dashbord') }}
@endsection


@section('content')

    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">{{ trans('admin.dashboardTitle') }}</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>{{ trans('admin.statistics') }}</span>
            </li>
        </ul>
    </div>

    <h1 class="page-title">  {{ trans('admin.statistics') }}
        <small>{{ trans('admin.statisticsView') }}</small>
    </h1>

    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 blue">
                <div class="visual">
                    <i class="fa fa-users"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span>{{ $categoriesCount }}</span>
                    </div>
                    <div class="desc"> {{ trans('admin.categoriesCount') }} </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 red">
                <div class="visual">
                    <i class="fa fa-news"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span>{{ $postsCount }}</span>
                    </div>
                    <div class="desc">{{ trans('admin.postsCount') }}  </div>
                </div>
            </a>
        </div>
        
    </div>
    
@endsection
