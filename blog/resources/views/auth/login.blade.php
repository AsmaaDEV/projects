@extends('layouts.auth')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('admin.loginPageTitle') }}</div>
                <div class="panel-body">

                    <form class="login-form" action="{{ route('admin.post.login') }}" method="post">
                         @include('layouts.alerts')

                        <div class="alert alert-danger hide">
                            <button class="close" data-close="alert"></button>
                            <span class="error_span"> أدخل البريد الإلكتروني أو كلمة المرور </span>
                        </div>

                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Email</label>
                            <input class="form-control form-control-solid placeholder-no-fix" type="email" placeholder="email" name="email" value="{{ old('email') }}" required />
                        </div>
                        <div class="form-group">
                            <label class="control-label visible-ie8 visible-ie9">Password</label>
                            <input class="form-control form-control-solid placeholder-no-fix" type="password" placeholder="password" name="password" required/>
                        </div>
                        <div class="form-actions">
                            <input type="hidden" name="_token" value="{{ Session::token() }}">

                            <button type="submit" class="btn green uppercase">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection