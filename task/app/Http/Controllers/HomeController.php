<?php

namespace App\Http\Controllers;

use App\User;
use App\Package;
use App\Subscription;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Charts;

class HomeController extends Controller
{
    public function home()
    {
        $usersCount = User::where('user_type', 'user')->count();

        $revenues = Subscription::sum('price');

        $usersChart = Charts::database(User::where('user_type', 'user')->get(), 'bar', 'highcharts')
                            ->elementLabel('Cilents')
                            ->title('Cilents chart')
                            ->dimensions(0, 200)
                            ->groupByMonth();

        $revenuesChart = Charts::database(Subscription::get(), 'bar', 'highcharts')
                            ->elementLabel('Revenues ')
                            ->title('Revenues Chart')
                            ->dimensions(0, 200)
                            ->groupByMonth();

        return view('welcome', compact(
                                        'usersCount','revenues',
                                        'revenuesChart', 'usersChart'
                                    ));
    }


}
