<?php

namespace App\Console;

use Illuminate\Support\Facades\Mail;
use App\Mail\EndSubscription;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Subscription;
use Carbon\Carbon;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    protected function sendEmail($userEmail, $headingTitle, $body) {

        $data = [
            'mailSubject'   => 'End sunbscriptin',
            'headingTitle'  => $headingTitle,
            'messagesTitle' => 'Server notification',
            'body'  => $body
        ];

        Mail::to($userEmail)->send(new EndSubscription($data));

        if( count(Mail::failures()) > 0 ) {
            Mail::to($userEmail)->send(new EndSubscription($data));

            if( count(Mail::failures()) > 0 ) {
                Mail::to($userEmail)->send(new EndSubscription($data));
            }
        }
    }
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            
            $subscriptions = Subscription::where('status', 1)
                                    ->get();
            foreach ($subscriptions as $subscription) {
                $startDate = Carbon::parse($subscription->start_date);

                $now = Carbon::now();

                $length = $now->diff($startDate);
                
                $days = $length->format('%a');

                $remainingDays = $subscription->package->duration - $days;
                
                if($remainingDays == 14){
                    $headingTitle = 'your package is nearing finishing'; 

                    $this->sendEmail($subscription->user->email, $headingTitle, $body);

                }

            }
        })->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
