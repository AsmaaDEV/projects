<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2> {{ $headingTitle }} </h2>
<div>
    <p>
        <span> body: </span><br>
        {{ $body }}
    </p><br>
   Thanks,<br>
{{ config('app.name') }} 
</div>
</body>
</html>