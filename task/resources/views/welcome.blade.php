<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Task</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        {!! Charts::assets() !!} 
       
    </head>
    <body>
        <div class="row">

            <div class="card">
                <div class="card__header">
                    <h3>Users Count  ({{ $usersCount }})</h3>
                    {!!  $usersChart->render() !!}
                </div>
            </div>
            <div class="card">
                <div class="card__header">
                    <h3>Revenues  ({{ $revenues }}) L.E</h3>
                    {!! $revenuesChart->render() !!}
                </div>
            </div>
            
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>

    </body>
</html>
